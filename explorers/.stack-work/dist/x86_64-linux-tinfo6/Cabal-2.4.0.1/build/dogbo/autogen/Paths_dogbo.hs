{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_dogbo (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,0,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/peikos/Git/dogbo/.stack-work/install/x86_64-linux-tinfo6/0d4607eaebd01ed2a1eab9db02fe6f317fa9118e02aa7dbf4dfc53ed1e868c05/8.6.5/bin"
libdir     = "/home/peikos/Git/dogbo/.stack-work/install/x86_64-linux-tinfo6/0d4607eaebd01ed2a1eab9db02fe6f317fa9118e02aa7dbf4dfc53ed1e868c05/8.6.5/lib/x86_64-linux-ghc-8.6.5/dogbo-0.0.0.0-5fepdExx8Jf82nG2vjgrbl-dogbo"
dynlibdir  = "/home/peikos/Git/dogbo/.stack-work/install/x86_64-linux-tinfo6/0d4607eaebd01ed2a1eab9db02fe6f317fa9118e02aa7dbf4dfc53ed1e868c05/8.6.5/lib/x86_64-linux-ghc-8.6.5"
datadir    = "/home/peikos/Git/dogbo/.stack-work/install/x86_64-linux-tinfo6/0d4607eaebd01ed2a1eab9db02fe6f317fa9118e02aa7dbf4dfc53ed1e868c05/8.6.5/share/x86_64-linux-ghc-8.6.5/dogbo-0.0.0.0"
libexecdir = "/home/peikos/Git/dogbo/.stack-work/install/x86_64-linux-tinfo6/0d4607eaebd01ed2a1eab9db02fe6f317fa9118e02aa7dbf4dfc53ed1e868c05/8.6.5/libexec/x86_64-linux-ghc-8.6.5/dogbo-0.0.0.0"
sysconfdir = "/home/peikos/Git/dogbo/.stack-work/install/x86_64-linux-tinfo6/0d4607eaebd01ed2a1eab9db02fe6f317fa9118e02aa7dbf4dfc53ed1e868c05/8.6.5/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "dogbo_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "dogbo_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "dogbo_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "dogbo_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "dogbo_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "dogbo_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
