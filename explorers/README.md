# dogbo

[![Hackage](https://img.shields.io/hackage/v/dogbo.svg?logo=haskell)](https://hackage.haskell.org/package/dogbo)
[![Stackage Lts](http://stackage.org/package/dogbo/badge/lts)](http://stackage.org/lts/package/dogbo)
[![Stackage Nightly](http://stackage.org/package/dogbo/badge/nightly)](http://stackage.org/nightly/package/dogbo)
[![MIT license](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)

See README for more info
