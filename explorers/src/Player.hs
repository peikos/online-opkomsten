{-# LANGUAGE TemplateHaskell, TypeFamilies, ScopedTypeVariables #-}

module Player where

import Control.Monad
import Data.List (nub)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (pack, toLower)
import qualified Data.Text as T
import Happstack.Server
import Text.Blaze.Html5 (Html, (!), toHtml)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import Text.Read (read)
import Text.Blaze.Html.Renderer.Text

data Character = Character { cPlayer :: Text
                           , cName :: Text
                           , cRole :: Text
                           , cTeam :: Team
                           }

data Team = Team { tName :: Text
                 , tChannel :: Text
                 , tPassword :: Password
                 }

newtype Password = Password { unPw :: Text }

password :: Text -> Password
password p | T.length p == 8 = Password p
           | otherwise     = error "password length"

teamToPuzzle :: Team -> Html
teamToPuzzle (Team { tPassword = pw } ) =
  do let [a,b,c,d,e,f,g,h] = T.unpack $ unPw pw :: [Char]
     let cell = H.td . toHtml . T.singleton
     H.table $ do H.tr $ do cell a
                            cell d
                            cell g
                  H.tr $ do cell f
                            cell 'x'
                            cell b
                  H.tr $ do cell c
                            cell h
                            cell e

printHtml :: Html -> IO ()
printHtml = print . renderHtml

template :: Text -> Html -> ServerPart Response
template title body =
  ship $ H.html $ do H.head $ do H.title $ toHtml $ title
                                 H.link ! A.href "/assets/style.css" ! A.type_ "text/css" ! A.rel "stylesheet"
                                 H.link ! A.href "https://fonts.googleapis.com/css2?family=Tulpen+One&display=swap" ! A.type_ "text/css" ! A.rel "stylesheet"
                     H.body $ body

par = H.p . toHtml

charPage :: ServerPart Response
charPage = do lidnr <- toLower . pack <$> look "lidnr"
              case Map.lookup lidnr players of
                Nothing -> emptyResp
                (Just (Character player name role team@(Team teamname channel password))) ->
                  template ("Welkom " <> name) $ H.body $ do H.h1 $ toHtml $ "Welkom " <> name
                                                             par $ "Beste " <> player <> ","
                                                             par $ "Lang geleden, iets meer dan 9000 jaar na het ontwaken van de mensheid, werd de wereld voor het eerst bezocht door buitenaards leven: de Elohim. Zij inspireerde drie volkeren om zich tot grote machten te ontwikkelen: de Rassenna, de Rem-En-Kim en de Ponnim. Hoewel deze volkeren er niet meer zijn, leven hun nazaten verder. In jou vloeit het bloed van de " <> teamname <> ", waarmee je een van de laatste telgen van een lange lijn bent. Voor jouw volk ben je bekend als " <> name <> ", " <> role <> " van de " <> teamname <> "."
                                                             par $ "Toen de Elohim vertrokken lieten zij een profetie achter, die indertijd niet begrepen werd: \"Aan het einde der tijden, als mensen thuiswerken en WC-papier het nieuwe goud is, zullen wij terugkeren. Dan zullen de nazaten van een van jullie volkeren zich moeten bewijzen door de heilige McGuffin te vinden en zo als enige volk van de stervende aarde door ons mee te zullen worden genomen naar een nieuw thuis tussen de sterren.\""
                                                             par $ "Het tijdperk waarvan de profetie spreekt is aangebroken. Jij bent als een van de meest kansrijke nazaten van jouw volk verkozen om de strijd voor de McGuffin aan te gaan. Als eerste challenge staat hieronder een wachtwoord verstopt om samen met de anderen van jouw volk in concatct te komen:"
                                                             teamToPuzzle team
                                                             H.p $ "Download " <> (H.a "TeamSpeak3" ! A.href "https://www.teamspeak.com") <> " en log in op " <> H.em "ts.peikos.net" <> "met de default channel " <> H.em ("DOGBO/" <> toHtml channel) <> " en het wachtwoord dat je gevonden hebt. Gebruik als nickname " <> H.em (toHtml name) <> ", de naam waarmee je volk je kent, zodat vijanden je ware identiteit niet kunnen achterhalen."
                                                             H.div ! A.id (H.textValue channel) $ ""

team1 = Team "Rasenna" "rasenna" $ password "Ethausva"
team2 = Team "Rem-En-Kum" "rem-en-kum" $ password "Menkaure"
team3 = Team "Ponnim" "ponnim" $ password "Melqarth"
oscar = Character "Oscar" "Thocero Saeru" "role" team1 -- Oscar
maurits = Character "Maurits" "Aranth Sherturi 2" "role" team1 -- Maurits
marilene = Character "Marilène" "Tetia Shelvashlia" "role" team1 -- Marilene
jelle = Character "Jelle" "Sa-Amen" "role" team2 -- Jelle
luuk = Character "Luuk" "Tan-To-Chnoubis" "role" team2 -- Luuk
mai = Character "Mai" "Shibanit" "role" team3 -- Mai
ties = Character "Ties" "Hammon" "role" team3 -- Ties
merlijn = Character "Merlijn" "Maharbaal" "role" team3 -- Merlijn
--mai = Character "Mai" "Ten-Makara" "role" team2 -- Mai

players :: Map Text Character
players = Map.fromList [("603693156", oscar), ("601280713", maurits), ("602229045", marilene), ("600839074", jelle), ("602402152", luuk), ("603360681", mai), ("601521734", ties), ("601574468", merlijn)]

fileServing :: ServerPart Response
fileServing = serveDirectory DisableBrowsing [] "assets"

ship :: Html -> ServerPart Response
ship = ok . toResponse

emptyResp = template "" ""

welkom = H.form ! A.action "/speler" $ do
  H.p $ "Geef je lidnummer hieronder"
  H.input ! A.id "c" ! A.name "lidnr" ! A.type_ "text"
  H.input ! A.type_ "submit" ! A.value "「 Login 」"

reisForm = template "Reislocatie invoeren" $ do H.h1 "Voer je reislocatie in: "
                                                form

maybeRead :: Read r => String -> Maybe r
maybeRead = fmap fst . listToMaybe . reads

type Coord = (Double, Double)

locResult = do input <- look "c"
               traceShow input $ case maybeRead $ "(" <> input <> ")" of
                 (Just (c :: Coord)) -> findLoc locs c
                 Nothing -> template "Fout" $ H.p "De computer heeft je coordinaat niet goed begrepen, probeer het nog eens!"


findLoc :: [(Coord, ServerPart Response)] -> Coord -> ServerPart Response
findLoc ((l,r):ls) c | close l c = r
                     | otherwise = findLoc ls c
findLoc [] _ = template "Fout" $ H.p "Dit is geen goede locatie, sorry. Probeer het nog eens."

loc = template "Locatie gevonden" . H.p

linkpage :: Text -> ServerPart Response
linkpage link = seeOther link $ toResponse link

plain = H.span ! A.style "font: Arial, sans-serif"

locs :: [(Coord, ServerPart Response)]
locs = [ ((52.138380, 5.205655), loc "Hier zijn we begonnen...")
       , ((50.813611, -2.474722), linkpage "https://www.youtube.com/playlist?list=PL1VtsTFlqPUQfc3gzebFOk_PnkKjvbYR2")
       , ((40.68, 117.23), loc $ plain "τ")
       , ((48.8738, 2.295), loc $ plain "ρ")
       , ((-22.951944, -43.210556), loc $ plain "ο")
       , ((-33.857058, 151.214897), loc $ plain "ι")
       , ((40.689167, -74.044444), loc $ plain "α")
       , ((39.9575, 26.238889), loc ozymandias)
       , ((25.7275, 32.6104), linkpage "https://i.redd.it/hed62pleqyrz.png")
       , ((38.250139, -122.410806), linkpage "/assets/wally.html")
       , ((49.901667, 15.932222), win)
       ]

win = linkpage "/assets/4221A0FC3DFBFD830DC3A13F6C72D233781179BEA27DF532FF903F3ABDBA5586.jpg"

ozymandias :: Html
ozymandias = H.div $ do H.p "I met a traveller from an antique land"
                        H.p "Who said: Two vast and trunkless legs of stone"
                        H.p "Stand in the desert. Near them, on the sand,"
                        H.p "..."

atan2 y x
   | x > 0            =  atan (y/x)
   | x == 0 && y > 0  =  pi/2
   | x <  0 && y > 0  =  pi + atan (y/x)
   |(x <= 0 && y < 0)            ||
    (x <  0 && isNegativeZero y) ||
    (isNegativeZero x && isNegativeZero y)
                      = -atan2 (-y) x
   | y == 0 && (x < 0 || isNegativeZero x)
                       =  pi    -- must be after the previous test on zero y
   | x==0 && y==0      =  y     -- must be after the other double zero tests
   | otherwise         =  x + y -- x or y is a NaN, return a NaN (via +)


close :: Coord -> Coord -> Bool
close a b = hav a b <= 100

hav :: Coord -> Coord -> Double
hav (lat1, lon1) (lat2, lon2) =
  let r = 6378.137;
      dLat = lat2 * pi/180 - lat1 * pi/180
      dLon = lon2 * pi/180 - lon1 * pi/180
      a = sin(dLat/2) * sin(dLat/2) + cos(lat1*pi/180) * cos(lat2*pi/180) * sin(dLon/2) * sin(dLon/2)
      c = 2 * atan2 (sqrt a) (sqrt $ 1 - a)
  in r * c * 1000

form :: H.Html
form = H.form ! A.action "/locatie" $ do
  H.p $ "Voer hieronder je locatie in, in decimaal coordinaat-formaat. Bijvoorbeeld: " <> H.em "52.138380, 5.205655"
  H.p $ "Positieve coordinaten zijn Noord en Oost, gebruik getallen voor West en Zuid."
  H.input ! A.id "c" ! A.name "c" ! A.type_ "text"
  H.input ! A.type_ "submit" ! A.value "「 Allons-y! 」"

tarchna = template "Security Check" $
  H.form ! A.action "/BF2B78B563020D5D1EEB5A09DB5E23482569D1D8403E176E29AB7C1121B57329" $ do
    H.p $ "Geef je channel-wachtwoord hieronder nog een keer:"
    H.input ! A.id "c" ! A.name "password" ! A.type_ "text"
    H.input ! A.type_ "submit" ! A.value "「 Check 」"

tanis = template "Security Check" $
  H.form ! A.action "/884BD25AA0ED28F77900DF05595A81B472D6605682581E4AAE452BE4A6820AF5" $ do
    H.p $ "Geef je channel-wachtwoord hieronder nog een keer:"
    H.input ! A.id "c" ! A.name "password" ! A.type_ "text"
    H.input ! A.type_ "submit" ! A.value "「 Check 」"

tyre = template "Security Check" $
  H.form ! A.action "/767E419E7FE373F163D4F570040EE782139AA65AEB2D3ECCD1953479A148C2B0" $ do
    H.p $ "Geef je channel-wachtwoord hieronder nog een keer:"
    H.input ! A.id "c" ! A.name "password" ! A.type_ "text"
    H.input ! A.type_ "submit" ! A.value "「 Check 」"

tyre2 = do pass <- look "password"
           if pass == "Melqarth"
             then continue
             else emptyResp

tarchna2 = do pass <- look "password"
              if pass == "Ethausva"
                then continue
                else emptyResp

tanis2 = do pass <- look "password"
            if pass == "Menkaure"
              then continue
              else emptyResp

continue = linkpage "/assets/Devil.jpg"

router :: IO ()
router = do let conf = nullConf { port = 9000 }
            putStrLn "Starting server"
            simpleHTTP conf $ msum [
                                     dir "assets" fileServing
                                   , dir "speler" charPage
                                   , dir "reis" reisForm
                                   , dir "locatie" locResult
                                   , dir "0EAB62188BA06A33E90165A580FF38B8CF7A0A82C38BB475181EA0C5FE6BC0E3" tanis
                                   , dir "52875A412C2A50AB27BDAF2F944FC0E87C27728105ACF69DD472EE3256480E51" tarchna
                                   , dir "48102CAE8AF275FB1F8ECF160DD9A4A2425FB981A2A3248310A57A7D64AA3186" tyre
                                   , dir "884BD25AA0ED28F77900DF05595A81B472D6605682581E4AAE452BE4A6820AF5" tanis2
                                   , dir "BF2B78B563020D5D1EEB5A09DB5E23482569D1D8403E176E29AB7C1121B57329" tarchna2
                                   , dir "767E419E7FE373F163D4F570040EE782139AA65AEB2D3ECCD1953479A148C2B0" tyre2
                                   , template "Welkom" welkom
                                   ]
