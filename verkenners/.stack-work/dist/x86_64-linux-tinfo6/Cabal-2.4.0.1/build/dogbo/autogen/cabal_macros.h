/* DO NOT EDIT: This file is automatically generated by Cabal */

/* package dogbo-0.0.0.0 */
#ifndef VERSION_dogbo
#define VERSION_dogbo "0.0.0.0"
#endif /* VERSION_dogbo */
#ifndef MIN_VERSION_dogbo
#define MIN_VERSION_dogbo(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  0 || \
  (major1) == 0 && (major2) == 0 && (minor) <= 0)
#endif /* MIN_VERSION_dogbo */

/* package base-noprelude-4.12.0.0 */
#ifndef VERSION_base_noprelude
#define VERSION_base_noprelude "4.12.0.0"
#endif /* VERSION_base_noprelude */
#ifndef MIN_VERSION_base_noprelude
#define MIN_VERSION_base_noprelude(major1,major2,minor) (\
  (major1) <  4 || \
  (major1) == 4 && (major2) <  12 || \
  (major1) == 4 && (major2) == 12 && (minor) <= 0)
#endif /* MIN_VERSION_base_noprelude */

/* package dogbo-0.0.0.0 */
#ifndef VERSION_dogbo
#define VERSION_dogbo "0.0.0.0"
#endif /* VERSION_dogbo */
#ifndef MIN_VERSION_dogbo
#define MIN_VERSION_dogbo(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  0 || \
  (major1) == 0 && (major2) == 0 && (minor) <= 0)
#endif /* MIN_VERSION_dogbo */

/* package relude-0.5.0 */
#ifndef VERSION_relude
#define VERSION_relude "0.5.0"
#endif /* VERSION_relude */
#ifndef MIN_VERSION_relude
#define MIN_VERSION_relude(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  5 || \
  (major1) == 0 && (major2) == 5 && (minor) <= 0)
#endif /* MIN_VERSION_relude */

/* tool c2hs-0.28.6 */
#ifndef TOOL_VERSION_c2hs
#define TOOL_VERSION_c2hs "0.28.6"
#endif /* TOOL_VERSION_c2hs */
#ifndef MIN_TOOL_VERSION_c2hs
#define MIN_TOOL_VERSION_c2hs(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  28 || \
  (major1) == 0 && (major2) == 28 && (minor) <= 6)
#endif /* MIN_TOOL_VERSION_c2hs */

/* tool cpphs-1.20.9 */
#ifndef TOOL_VERSION_cpphs
#define TOOL_VERSION_cpphs "1.20.9"
#endif /* TOOL_VERSION_cpphs */
#ifndef MIN_TOOL_VERSION_cpphs
#define MIN_TOOL_VERSION_cpphs(major1,major2,minor) (\
  (major1) <  1 || \
  (major1) == 1 && (major2) <  20 || \
  (major1) == 1 && (major2) == 20 && (minor) <= 9)
#endif /* MIN_TOOL_VERSION_cpphs */

/* tool gcc-10.1.0 */
#ifndef TOOL_VERSION_gcc
#define TOOL_VERSION_gcc "10.1.0"
#endif /* TOOL_VERSION_gcc */
#ifndef MIN_TOOL_VERSION_gcc
#define MIN_TOOL_VERSION_gcc(major1,major2,minor) (\
  (major1) <  10 || \
  (major1) == 10 && (major2) <  1 || \
  (major1) == 10 && (major2) == 1 && (minor) <= 0)
#endif /* MIN_TOOL_VERSION_gcc */

/* tool ghc-8.6.5 */
#ifndef TOOL_VERSION_ghc
#define TOOL_VERSION_ghc "8.6.5"
#endif /* TOOL_VERSION_ghc */
#ifndef MIN_TOOL_VERSION_ghc
#define MIN_TOOL_VERSION_ghc(major1,major2,minor) (\
  (major1) <  8 || \
  (major1) == 8 && (major2) <  6 || \
  (major1) == 8 && (major2) == 6 && (minor) <= 5)
#endif /* MIN_TOOL_VERSION_ghc */

/* tool ghc-pkg-8.6.5 */
#ifndef TOOL_VERSION_ghc_pkg
#define TOOL_VERSION_ghc_pkg "8.6.5"
#endif /* TOOL_VERSION_ghc_pkg */
#ifndef MIN_TOOL_VERSION_ghc_pkg
#define MIN_TOOL_VERSION_ghc_pkg(major1,major2,minor) (\
  (major1) <  8 || \
  (major1) == 8 && (major2) <  6 || \
  (major1) == 8 && (major2) == 6 && (minor) <= 5)
#endif /* MIN_TOOL_VERSION_ghc_pkg */

/* tool haddock-2.22.0 */
#ifndef TOOL_VERSION_haddock
#define TOOL_VERSION_haddock "2.22.0"
#endif /* TOOL_VERSION_haddock */
#ifndef MIN_TOOL_VERSION_haddock
#define MIN_TOOL_VERSION_haddock(major1,major2,minor) (\
  (major1) <  2 || \
  (major1) == 2 && (major2) <  22 || \
  (major1) == 2 && (major2) == 22 && (minor) <= 0)
#endif /* MIN_TOOL_VERSION_haddock */

/* tool happy-1.19.12 */
#ifndef TOOL_VERSION_happy
#define TOOL_VERSION_happy "1.19.12"
#endif /* TOOL_VERSION_happy */
#ifndef MIN_TOOL_VERSION_happy
#define MIN_TOOL_VERSION_happy(major1,major2,minor) (\
  (major1) <  1 || \
  (major1) == 1 && (major2) <  19 || \
  (major1) == 1 && (major2) == 19 && (minor) <= 12)
#endif /* MIN_TOOL_VERSION_happy */

/* tool hpc-0.67 */
#ifndef TOOL_VERSION_hpc
#define TOOL_VERSION_hpc "0.67"
#endif /* TOOL_VERSION_hpc */
#ifndef MIN_TOOL_VERSION_hpc
#define MIN_TOOL_VERSION_hpc(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  67 || \
  (major1) == 0 && (major2) == 67 && (minor) <= 0)
#endif /* MIN_TOOL_VERSION_hpc */

/* tool hsc2hs-0.68.5 */
#ifndef TOOL_VERSION_hsc2hs
#define TOOL_VERSION_hsc2hs "0.68.5"
#endif /* TOOL_VERSION_hsc2hs */
#ifndef MIN_TOOL_VERSION_hsc2hs
#define MIN_TOOL_VERSION_hsc2hs(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  68 || \
  (major1) == 0 && (major2) == 68 && (minor) <= 5)
#endif /* MIN_TOOL_VERSION_hsc2hs */

/* tool hscolour-1.24 */
#ifndef TOOL_VERSION_hscolour
#define TOOL_VERSION_hscolour "1.24"
#endif /* TOOL_VERSION_hscolour */
#ifndef MIN_TOOL_VERSION_hscolour
#define MIN_TOOL_VERSION_hscolour(major1,major2,minor) (\
  (major1) <  1 || \
  (major1) == 1 && (major2) <  24 || \
  (major1) == 1 && (major2) == 24 && (minor) <= 0)
#endif /* MIN_TOOL_VERSION_hscolour */

/* tool pkg-config-1.7.3 */
#ifndef TOOL_VERSION_pkg_config
#define TOOL_VERSION_pkg_config "1.7.3"
#endif /* TOOL_VERSION_pkg_config */
#ifndef MIN_TOOL_VERSION_pkg_config
#define MIN_TOOL_VERSION_pkg_config(major1,major2,minor) (\
  (major1) <  1 || \
  (major1) == 1 && (major2) <  7 || \
  (major1) == 1 && (major2) == 7 && (minor) <= 3)
#endif /* MIN_TOOL_VERSION_pkg_config */

/* tool runghc-8.6.5 */
#ifndef TOOL_VERSION_runghc
#define TOOL_VERSION_runghc "8.6.5"
#endif /* TOOL_VERSION_runghc */
#ifndef MIN_TOOL_VERSION_runghc
#define MIN_TOOL_VERSION_runghc(major1,major2,minor) (\
  (major1) <  8 || \
  (major1) == 8 && (major2) <  6 || \
  (major1) == 8 && (major2) == 6 && (minor) <= 5)
#endif /* MIN_TOOL_VERSION_runghc */

/* tool strip-2.34 */
#ifndef TOOL_VERSION_strip
#define TOOL_VERSION_strip "2.34"
#endif /* TOOL_VERSION_strip */
#ifndef MIN_TOOL_VERSION_strip
#define MIN_TOOL_VERSION_strip(major1,major2,minor) (\
  (major1) <  2 || \
  (major1) == 2 && (major2) <  34 || \
  (major1) == 2 && (major2) == 34 && (minor) <= 0)
#endif /* MIN_TOOL_VERSION_strip */

#ifndef CURRENT_COMPONENT_ID
#define CURRENT_COMPONENT_ID "dogbo-0.0.0.0-5fepdExx8Jf82nG2vjgrbl-dogbo"
#endif /* CURRENT_COMPONENT_ID */
#ifndef CURRENT_PACKAGE_VERSION
#define CURRENT_PACKAGE_VERSION "0.0.0.0"
#endif /* CURRENT_PACKAGE_VERSION */
