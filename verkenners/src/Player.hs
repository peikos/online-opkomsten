{-# LANGUAGE TemplateHaskell, TypeFamilies, ScopedTypeVariables #-}

module Player where

import Control.Monad
import Data.List (nub)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (pack, toLower)
import qualified Data.Text as T
import Happstack.Server
import Text.Blaze.Html5 (Html, (!), toHtml)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import Text.Read (read)
import Text.Blaze.Html.Renderer.Text

data Character = Character { cPlayer :: Text
                           , cName :: Text
                           , cRole :: Text
                           , cTeam :: Team
                           }

data Team = Team { tName :: Text
                 , tChannel :: Text
                 , tPassword :: Password
                 }

newtype Password = Password { unPw :: Text }

password :: Text -> Password
password p | T.length p == 8 = Password p
           | otherwise     = error "password length"

teamToPuzzle :: Team -> Html
teamToPuzzle (Team { tPassword = pw } ) =
  do let [a,b,c,d,e,f,g,h] = T.unpack $ unPw pw :: [Char]
     let cell = H.td . toHtml . T.singleton
     H.table $ do H.tr $ do cell a
                            cell d
                            cell g
                  H.tr $ do cell f
                            cell 'x'
                            cell b
                  H.tr $ do cell c
                            cell h
                            cell e

printHtml :: Html -> IO ()
printHtml = print . renderHtml

template :: Text -> Html -> ServerPart Response
template title body =
  ship $ H.html $ do H.head $ do H.title $ toHtml $ title
                                 H.link ! A.href "/assets/style.css" ! A.type_ "text/css" ! A.rel "stylesheet"
                                 H.link ! A.href "https://fonts.googleapis.com/css2?family=Tulpen+One&display=swap" ! A.type_ "text/css" ! A.rel "stylesheet"
                     H.body $ body


fileServing :: ServerPart Response
fileServing = serveDirectory DisableBrowsing [] "assets"

ship :: Html -> ServerPart Response
ship = ok . toResponse

emptyResp = template "" ""

reisForm = template "Reislocatie invoeren" $ do H.h1 "Voer je reislocatie in: "
                                                H.p $ "Voer hieronder je locatie in, in decimaal coordinaat-formaat. Bijvoorbeeld: " <> H.em "52.138380, 5.205655"
                                                H.p $ "Positieve coordinaten zijn Noord en Oost, gebruik negatieve getallen voor West en Zuid."
                                                H.p $ "Als je wel weet waar je zijn moet, maar geen kloppend coordinaat kan vinden, kijk op Wikipedia en volg de coordinaat-link rechtsboven naar GeoHack."
                                                form

form :: H.Html
form = H.form ! A.action "/locatie" $ do
  H.input ! A.id "c" ! A.name "c" ! A.type_ "text"
  H.input ! A.type_ "submit" ! A.value "「 Allons-y! 」"

maybeRead :: Read r => String -> Maybe r
maybeRead = fmap fst . listToMaybe . reads

type Coord = (Double, Double)

locResult = do input <- look "c"
               traceShow input $ case maybeRead $ "(" <> input <> ")" of
                 (Just (c :: Coord)) -> findLoc locs c
                 Nothing -> template "Fout" $ H.p "De computer heeft je coordinaat niet goed begrepen, probeer het nog eens!"


findLoc :: [(Coord, ServerPart Response)] -> Coord -> ServerPart Response
findLoc ((l,r):ls) c | close l c = r
                     | otherwise = findLoc ls c
findLoc [] _ = template "Fout" $ H.p "Dit is geen goede locatie, sorry. Probeer het nog eens."

loc t = template "Locatie gevonden" $ do H.h2 "Goed bezig! Dit is je volgende clue:"
                                         H.p t
                                         H.h3 "Voer je volgende reislocatie in: "
                                         form

linkpage :: Text -> ServerPart Response
linkpage link = seeOther link $ toResponse link

plain = H.span ! A.style "font: Arial, sans-serif"

atan2 y x
   | x > 0            =  atan (y/x)
   | x == 0 && y > 0  =  pi/2
   | x <  0 && y > 0  =  pi + atan (y/x)
   |(x <= 0 && y < 0)            ||
    (x <  0 && isNegativeZero y) ||
    (isNegativeZero x && isNegativeZero y)
                      = -atan2 (-y) x
   | y == 0 && (x < 0 || isNegativeZero x)
                       =  pi    -- must be after the previous test on zero y
   | x==0 && y==0      =  y     -- must be after the other double zero tests
   | otherwise         =  x + y -- x or y is a NaN, return a NaN (via +)


close :: Coord -> Coord -> Bool
close a b = hav a b <= 1000

hav :: Coord -> Coord -> Double
hav (lat1, lon1) (lat2, lon2) =
  let r = 6378.137;
      dLat = lat2 * pi/180 - lat1 * pi/180
      dLon = lon2 * pi/180 - lon1 * pi/180
      a = sin(dLat/2) * sin(dLat/2) + cos(lat1*pi/180) * cos(lat2*pi/180) * sin(dLon/2) * sin(dLon/2)
      c = 2 * atan2 (sqrt a) (sqrt $ 1 - a)
  in r * c * 1000

router :: IO ()
router = do let conf = nullConf { port = 9000 }
            putStrLn "Starting server"
            simpleHTTP conf $ msum [
                                     dir "assets" fileServing
                                   , dir "locatie" locResult
                                   , dir "F8BDD3803A448C96CAE32119E3B4910F619421189372CAB34EA7A03A961921DD" $ loc bosch1
                                   , dir "33E464CAC145E746DF483501BF89D408291C54C18691FA61E53BF674004B7BB9" $ loc bosch2
                                   , reisForm
                                   ]

locs :: [(Coord, ServerPart Response)]
locs = [ ((52.138380, 5.205655), loc "Hier zijn we begonnen... ZW Z Z ZO L L R R L 2⬆️ NNW PROF L R Rest?")
       , ((49.05, 3.4) {- Château-Thierry -}, loc kerk_vos)
       , ((51.755, -1.2627) {- Beaumont Palace -}, loc kerk_leeuw)
       , ((51.0666667, 8.8666667) {- Donareik Geismar -}, loc kerk_eekhoorn)
       , ((34.9229167, 138.6020833) {- Rundal Mall Adelaide -}, loc kerk_houtduif)
       , ((0, 37) {- Ol Pejeta Conservancy -}, loc kerk_kader)
       , ((52.090833, 5.122222) {- domtoren -}, linkpage "/assets/missing.pdf")
       , ((54.907658, 8.316756) {- niels -}, linkpage "/assets/garden.html" )
       , ((53.032594, 5.663708) {- fenna -}, linkpage "/assets/garden.html" )
       , ((42.35717, 14.404457) {- tom -}, linkpage "/assets/garden.html" )
       , ((43.074722, 12.605556) {- frank -}, linkpage "/assets/garden.html" )
       , ((24.465706, 39.610392) {- kalief -}, linkpage "https://www.youtube.com/playlist?list=PL1VtsTFlqPUQfc3gzebFOk_PnkKjvbYR2" )
       , ((24.467561, 39.611439) {- kalief -}, linkpage "https://www.youtube.com/playlist?list=PL1VtsTFlqPUQfc3gzebFOk_PnkKjvbYR2" )
       , ((38.958889, -84.389167) {- alexandria -}, loc voynich)
       , ((31.292778, -92.458889) {- alexandria -}, loc voynich)
       , ((45.883889, -95.370278) {- alexandria -}, loc voynich)
       , ((38.816111, -77.071111 ) {- alexandria -}, loc voynich)
       , ((41.31161, -72.92722) {- locatie voynich -}, loc nijl)
       , ((12.016667, 37.291944) {- begin nijl -}, loc begin)
       , ((-1.066667, 32.866667) {- begin nijl -}, loc begin)
       , ((-2.49, 29.283333) {- begin nijl -}, loc begin)
       , ((37.638228, 21.630793) {- beeld Zeus -}, loc petra)
       , ((30.328889, 35.440278) {- petra -}, loc nemo)
       , ((-48.876667, -123.393333) {- point nemo -}, win)
       , ((52.1604503,5.2013957) {- mauritshoeve -}, linkpage "/assets/discord.pdf")
       , ((21.315, -157.862) {- honululu -}, loc metamorphosis)
       ]

win = linkpage "/assets/4221A0FC3DFBFD830DC3A13F6C72D233781179BEA27DF532FF903F3ABDBA5586.pdf"

kerk_vos = H.div $ do H.p "Een van de mensen met wie jullie PL haar naam deelt, was een schrijfster van zedelijke lectuur voor vrouwen en huisselijke kringen. Ze groeide op in een herenhuis in een Fries dorpje - welke? "
kerk_leeuw = H.div $ do H.p "De naam van jullie PL is tevens die van een belangrijke Bijbelse heilige. Waar staat zijn basiliek?"
kerk_houtduif = H.div $ do H.p "Jullie PL deelt zijn naam met Sint Nicolaas, aan wie vele kerken zijn opgedragen. Op een Duits eiland staat een kerk die specifiek ook echt naar Sint Niels vermoemd is. Welke?"
kerk_eekhoorn = H.div $ do H.p "De naam van jullie PL is een variatie op die van een heilige die een belangrijke kloosterorde heeft opgericht. Waar staat zijn basiliek?"
kerk_kader = H.div $ do H.p "Jullie patrouilles zijn deze ronde (met een enkele uitzondering) op zoek naar verschillende kerken, maar wij houden het dichter in de buurt. Welke kathedraal met bekende toren is gebouwd als hoofdkerk van de Nederlanden?"
begin = H.div $ do H.p "Waar zijn we begonnen?"

bosch1 = H.div $ do H.p "Waar ligt de eerste Rashidun kalief begraven?"
bosch2 = H.div $ do H.p "Er zijn veel steden direct of indirect vernoemd naar Alexander de Grote. Vind er eentje die ver buiten zijn veroverde gebied ligt."
nijl = H.div $ do H.p "Een van de oudste beschaving rond de Middelandse Zee ontstond waar deze rivier uitmondt. Maar waar begint de rivier?"
nemo = H.div $ do H.p "Voor deze vragen zoeken we de middle of nowhere. Letterlijk. Welk punt op aarde (of specifieker, op zee) ligt het verst van land vandaan?"
voynich = H.div $ do H.p "Hieronder een pagina uit een mysterieus manuscript van lang geleden. Waar ligt dit werk nu?"
                     H.img ! A.src "/assets/manuscript.jpg"
metamorphosis = H.div $ do H.p "Wie heeft dit gemaakt?"
                           H.img ! A.src "/assets/metamorphosis.jpg"
petra = H.div $ do H.p "Deze eeuwenoude stad in Jordanië staat bekend om de in de rosten uitgehakte graftombes en was ooit de hoofdstad van de Nabateeën."
